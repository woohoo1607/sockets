import * as config from "./config";
import {texts} from "../data";
import {MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME} from "./config";
let activeClientsName = [];
let ids = 3;
let rooms = []
let gameTimers = {};
export default io => {
  io.on("connection", socket => {
    const id = socket.handshake.query.id=='false' ? socket.id : socket.handshake.query.id;
    const username = socket.handshake.query.username;
    const isUniqueName = activeClientsName.findIndex(user => user.username===username)===-1;
    const thisConnection = activeClientsName.findIndex(user => user.id===id)!==-1;


    if (isUniqueName) {
      socket.emit("SUCCESS_CONNECT", {success: true, id: id})
      activeClientsName.push({
        username: username,
        id: id,
      });
    } else if (!isUniqueName && thisConnection) {
      socket.emit("SUCCESS_CONNECT", {success: true, id: id})
    } else {
      socket.emit("SUCCESS_CONNECT", {success: false})
    }

    socket.emit("UPDATE_ROOMS", rooms);

    socket.on("CREATE_ROOM", data => {
      let isUnique = rooms.findIndex(room=> room.name===data.roomName)===-1;
      if (!isUnique) {
        io.emit("ERRORS", `Комната с именем ${data.roomName} уже существует!`)
      } else {
        rooms.push({id: ids, name: data.roomName, users: [], winners: [], isAvailable: true});
        io.emit("UPDATE_ROOMS", rooms);
        ids++;
        socket.join(ids-1, () => {
          const room = rooms.filter(room => room.id===(ids-1))[0];
          room.users.push({username: username, progressBar: '0%', isReady: false});
          io.to(ids-1).emit("JOIN_ROOM_DONE", {...room});
        });

      }
    });

    socket.on("CHANGE_READY", (roomId, user) => {

      const index = rooms.findIndex(r => r.id===roomId);
      const usrIndex = rooms[index].users.findIndex(u => u.username===user.username);
      rooms[index].users[usrIndex] = user;
      const isAllReady = (rooms[index].users.findIndex(u => u.isReady===false)===-1);
      io.to(roomId).emit("UPDATE_ROOM", {...rooms[index]});
      if (isAllReady && rooms[index].users.length>1) {
        startTimer(roomId);
      }
    });

    socket.on("JOIN_ROOM", (roomId, username) => {
      socket.join(roomId, () => {
        const room = rooms.filter(room => room.id===roomId)[0];
        room.users.push({username: username, progressBar: '0%', isReady: false});
        if (room.users.length===MAXIMUM_USERS_FOR_ONE_ROOM) {
          room.isAvailable = false;
        }
        io.to(roomId).emit("JOIN_ROOM_DONE", {...room});
        io.emit("UPDATE_ROOMS", rooms);
      });
    });

    socket.on("LEAVE_ROOMS", (id, username) => {
      const roomIndx = rooms.findIndex(room => room.id===id);
      if (rooms[roomIndx].users.length===1) {
        socket.leave(id);
        rooms.splice(roomIndx,1);
        io.emit("UPDATE_ROOMS", rooms);
      } else {
        let userIndex = rooms[roomIndx].users.findIndex(user=>user.username===username);
        let winnerIndex = rooms[roomIndx].winners.findIndex(user=>user.username===username);
        rooms[roomIndx].users.splice(userIndex, 1);
        rooms[roomIndx].winners.splice(winnerIndex, 1);
        rooms[roomIndx].isAvailable = true;
        io.emit("UPDATE_ROOMS", rooms);
        const isAllReady = (rooms[roomIndx].users.findIndex(u => u.isReady===false)===-1);
        io.to(id).emit("UPDATE_ROOM", {...rooms[roomIndx]});
        socket.leave(id);
        if (rooms[roomIndx].users.length>1 && isAllReady) {
          startTimer(id);
        }
      }
    });

    const startTimer = (roomId) => {
      const roomIndex = rooms.findIndex(room => room.id===roomId);
      rooms[roomIndex].isAvailable = false;
      io.emit("UPDATE_ROOMS", rooms);
      let timer = SECONDS_TIMER_BEFORE_START_GAME;
      const textId = Math.floor(Math.random() * texts.length);
      io.to(roomId).emit("START_TIMER", timer, textId);
      let time = setInterval(()=> {
        if(timer===0) {
          clearInterval(time);
        }
        io.to(roomId).emit("UPDATE_TIMER", timer, roomId);
        timer--;
      }, 1000);
    };

    socket.on("START_GAME", (id, username) => {
      const roomIndex = rooms.findIndex(room => room.id===id);
      rooms[roomIndex].winners = [];
      rooms[roomIndex].isAvailable = false;
      let timer = SECONDS_FOR_GAME;
      io.emit("UPDATE_ROOMS", rooms);
      io.to(id).emit("START_GAME_TIMER", timer);
      if (!gameTimers[id]) {
        gameTimers[id] = setInterval(() => {
          if (timer === 0) {
            clearInterval(gameTimers[id]);
            let res = '';
            rooms[roomIndex].winners.forEach((win, i) => {
              let number = +i + 1;
              res = res + number + ' - ' + win + '\n'
            });
            io.to(id).emit("END_GAME", res, id);
          }
          io.to(id).emit("UPDATE_GAME_TIMER", timer);
          timer--;
        }, 1000);
      }
      socket.on("CHANGE_PROGRESS", (roomId, username, progress) => {
        const index = rooms.findIndex(r => r.id===roomId);
        const usrIndex = rooms[index].users.findIndex(u => u.username===username);
        rooms[index].users[usrIndex].progressBar = progress;
        if (progress==='100%') {
          rooms[roomIndex].winners.push(username);
        }
        const isAllFinished = (rooms[index].users.findIndex(u => u.progressBar!=='100%')===-1);
        io.to(roomId).emit("UPDATE_ROOM", {...rooms[index]});
        if (isAllFinished) {

          clearInterval(gameTimers[roomId]);
          let res = '';
          rooms[roomIndex].winners.forEach((win, i) => {
            let number = +i+1;
            res = res + number + ' - ' + win + '\n'
          });
          if (rooms[roomIndex].users.length!==MAXIMUM_USERS_FOR_ONE_ROOM) {
            rooms[roomIndex].isAvailable = true;
            io.emit("UPDATE_ROOMS", rooms);
          }
          io.to(roomId).emit("END_GAME", res, roomId);
        }
      })
    });

  });

};
